package com.ef;

import com.ef.utils.DbUtils;
import com.ef.utils.ReadFileInsertTable;
import com.ef.utils.Validator;

public class Parser {
	static String dtFmt = "yyyy-MM-dd.HH:mm:ss";
	public static void main(String[] args) {
		String startDate = null;
		String duration = null;
		String accessLog = null;
		int threshold = -1;
		String user = "parser";
		String password = "parser";
		String url = "jdbc:mysql://localhost:3306/temp";
		
		boolean invalidParam = false;
		
		if (args.length < 3) {
			printUsage();
			return;
		} else {
			Validator validator = new Validator();
			String[] cmdValue = args[0].split("=");
			
			for (int i = 0; i < args.length && !invalidParam ; i++) {
				cmdValue = args[i].split("=");
				if (cmdValue[0].equals("--startDate")) {
					startDate = cmdValue[1];
					if (!validator.isDateValid(startDate, dtFmt)) {
						invalidParam = true;
					}
				}
				if (cmdValue[0].equals("--duration")) {
					duration = cmdValue[1];
					if (!validator.isDurationValid(duration)) {
						invalidParam = true;
					}
				}
				if (cmdValue[0].equals("--threshold")) {
				
					try {
						threshold = Integer.parseInt(cmdValue[1]);						
					} catch (Exception e) {
						invalidParam = true;
					}
				}
				if (cmdValue[0].equals("--accesslog")) {
					accessLog = cmdValue[1];
				}
			}
			
			if ( duration == null || startDate == null) {
				invalidParam = true;
			}
			
			if (invalidParam) {
				printUsage();
				return;
			}
					
			ReadFileInsertTable readFunc = new ReadFileInsertTable();
			
			DbUtils dbUtils = new DbUtils(url, user, password);
			if (accessLog != null && accessLog.trim().length() > 0)
			  readFunc.readFunc(accessLog, dbUtils.getConnection());
			dbUtils.getData(startDate, threshold, duration);
		}
	}
	private static void printUsage() {
		System.out.println("Usage : " );
		System.out.println("java -cp parser.jar com.ef.Parser ");
		System.out.print(" --startDate=" + dtFmt);
		System.out.print(" --duration=(horuly or daily");
		System.out.print(" --threshold= (any integer)");
		System.out.println("");		
	}
}

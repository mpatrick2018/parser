# Parser

Deliverables
------------

(1) Java program that can be run from command line
	
    java -cp "parser.jar" com.ef.Parser --accesslog=/path/to/file --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100 
Attached. 


(2) Source Code for the Java program
Attached.


(3) MySQL schema used for the log data
DDL From logdata table:
CREATE TABLE `logdata` (
  `dateId` timestamp NULL DEFAULT NULL,
  `ip` varchar(300) DEFAULT NULL,
  `request` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `userAgent` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DDL For blockedIp table:
CREATE TABLE `blockedIp` (
  `ipAddress` varchar(20) DEFAULT NULL,
  `comments` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





(4) SQL queries for SQL test


(A) Write MySQL query to find IPs that mode more than a certain number of requests for a given time period.

    Ex: Write SQL to find IPs that made more than 100 requests starting from 2017-01-01.13:00:00 to 2017-01-01.14:00:00.
    SQL to find IPs with more than x number of reqeuest for a given time period

	Select IP from (
	  select Count(*) as cnt, IP from logdata where dateId >= '2017-01-01.13:00:00' and dateId <= '2017-01-01.14:00:00' Group By IP 
	) as tmp Where cnt >= 100



(B) Write MySQL query to find requests made by a given IP.

	SQL to select an ip from the table 

	SELECT * from logdata WHERE IP = '<ip value>';
	  EX: 
	  SELECT * FROM logdata WHERE IP = '192.168.143.177';


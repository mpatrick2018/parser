package com.ef.utils;


import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

public class DbUtils {

	private String dbUrl = null;
	private String user = null;
	private String password = null;
	private Connection conn = null;
	private String className = "com.mysql.jdbc.Driver";
	
	public DbUtils(String url, String user, String password) {
		this.user = user;
		this.password = password;
		this.dbUrl = url;
	}
	
	
	/* get the connection */
	public Connection getConnection () {
		
		if (conn != null) {
			return conn;
		}
		if (dbUrl != null && user != null && password != null) {
			try {
				Class.forName(className);
				conn = DriverManager.getConnection(dbUrl, user, password);	
			} catch (SQLException ex) {	
				System.out.println("Sql Error " + ex.getMessage());
			} catch (ClassNotFoundException e) {
				System.out.println("ClassNotFoundException " + e.getMessage());
			}
		}
		return conn;
	}
	
	/* Close the conneciton */
	public void closeConn() {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException e) {
				System.out.println("Exception closing sql connection : " + e.getMessage());
			}	
		}
	}
	
	/* Get the ip addresses */
	public void getData(String startDate, int threshold, String duration ) {
		String sql = "SELECT IP From ";
			   sql += "(SELECT COUNT(*) as cnt, IP FROM logdata WHERE dateId >= ? AND dateId <= ? GROUP BY IP) as tmp";
			   sql += " WHERE cnt >= ?";
		
			   
		String sqlInsert = "Insert into blockedIp VALUES( ?, ?)";
		
		
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");
			   
	    java.sql.Timestamp tsStart = null;
	    java.sql.Timestamp tsEnd = null;
    	Calendar cTime = Calendar.getInstance();
    	
    	try {
    		cTime.setTime(sdf.parse(startDate));
		} catch (ParseException e) {
			System.out.println("Exception converting date " + e.getMessage());
			return;
		}
	    
	    // instructions have hourly as 13:00:00 to 14:00:00 in one spot and as 13:00:00 to 13:59:59 in another. Will make it the later so we don't get
    	// the next hour.
	    if (duration.equals("hourly")) {
    		tsStart = new java.sql.Timestamp(cTime.getTimeInMillis());
    		cTime.add(Calendar.HOUR_OF_DAY, 1);
    		cTime.add(Calendar.SECOND, -1);
    		tsEnd = new java.sql.Timestamp(cTime.getTimeInMillis());    		
	    } else {
    		tsStart = new java.sql.Timestamp(cTime.getTimeInMillis());
    		cTime.add(Calendar.HOUR_OF_DAY, 24);
    		cTime.add(Calendar.SECOND, -1);
    		tsEnd = new java.sql.Timestamp(cTime.getTimeInMillis());
	    }
	    String comment = "Blocked too many requests ("  + threshold + ") for time period ( " + tsStart + "   to  " + tsEnd +")" ;
	    
		try {
			if (conn == null) 
				getConnection();
			PreparedStatement findData = conn.prepareStatement(sql);
			PreparedStatement insertData = conn.prepareStatement(sqlInsert);
			
			findData.setTimestamp(1, tsStart);
			findData.setTimestamp(2, tsEnd);
			findData.setInt(3, threshold);
			ResultSet rs = findData.executeQuery();
			while (rs.next()) {
			  String ip = rs.getString("ip");
			  System.out.println(ip);
			  
				try {
					  insertData.setString(1, ip);
					  insertData.setString(2,  comment);
					  insertData.executeUpdate();	
				} catch (Exception e) {
					// in case the blockedip table was not created. Instructions were not clear on this point.
					// it it was not created it will ignore the exception. In a real program you would want to
					// log the error and correct it.
				}
			}
			
		} catch (SQLException ex) {	
			System.out.println("Sql Error " + ex.getMessage());
		} finally {
		}
		
	}
}

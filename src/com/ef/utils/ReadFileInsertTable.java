package com.ef.utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class ReadFileInsertTable {
	private DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	/* Open and read the file */
	public void readFunc(String path, Connection conn) {
		File file = new File(path);
		Scanner sc = null;
		
		String sql = "insert into logdata values(?, ?, ?, ?, ?)";
		try {
			PreparedStatement insertData = conn.prepareStatement(sql);
			sc  = new Scanner(file);
			while (sc.hasNextLine()) {
				String str = sc.nextLine();
				parseLine(str, insertData);
			}			
		} catch (SQLException ex) {	
			System.out.println("Sql Error " + ex.getMessage());
		} catch (IOException exp) {
			System.out.println("IOException " + exp.getMessage());
		} finally {
			sc.close();
		}
	}
	
	/* Parse each line */
	private void parseLine(String str, PreparedStatement insertData) {
		String dateStr, ipStr, requestStr, statuStr, userAgentStr;
		Scanner sc = new Scanner(str);
		sc.useDelimiter("[|]");
		try {
			
			while (sc.hasNext()) {
				dateStr = sc.next();
				ipStr = sc.next();
				requestStr = sc.next();
				statuStr = sc.next();
				userAgentStr = sc.next();
								
				try {
					java.sql.Timestamp ts = new java.sql.Timestamp(sdf.parse(dateStr).getTime());
					
					int statusInt = Integer.parseInt(statuStr);
					insertData.setTimestamp(1, ts);
					insertData.setString(2,  ipStr);
					insertData.setString(3, requestStr);
					insertData.setInt(4, statusInt);
					insertData.setString(5, userAgentStr);
					insertData.executeUpdate();
				} catch (SQLException e) {
					System.out.println("SQL Exception inserting line starting with: " + dateStr + "|" + ipStr + "  " + e.getMessage());
				} catch (ParseException e) {
					// invalid date. 
					System.out.println("Parsing error for line starting with : " + dateStr + "|" + ipStr);
				}	
			}			
		}finally {
			sc.close();
		}
	}
	
	
}

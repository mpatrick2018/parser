package com.ef.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Validator {
	
	
	/* Validate the date format. */
	public boolean isDateValid(String dateToValidate, String dateFmt) {
		boolean retVal = false;
		if(dateToValidate != null){			
			SimpleDateFormat sdf = new SimpleDateFormat(dateFmt);
			sdf.setLenient(false);
			try {
				//if not valid, it will throw ParseException
				sdf.parse(dateToValidate);
				retVal = true;
			} catch (ParseException e) {
				retVal = false;
			}
		}
		return retVal;
	}	
	
	/* validate the duration can only be "hourly" or "daily" */
	public boolean isDurationValid(String durationToValidate) {
		boolean retVal = false;	
		if (durationToValidate != null &&  (durationToValidate.equalsIgnoreCase("hourly") || durationToValidate.equalsIgnoreCase("daily"))) {
			retVal = true;
		}
		return retVal;
	}
	
}

package com.ef.tests.utils;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import com.ef.utils.Validator;

class ValidatorTests {
	private Validator validator;
	private String dtFmt = "yyyy-MM-dd.HH:mm:ss";

	
	
	@Before
	public void init() {
		validator = new Validator();
	}

	
	@Test
	void testDateIsNull() {
		validator = new Validator();
		assertFalse(validator.isDateValid(null, dtFmt));
	}
	
	@Test
	public void testDayIsInvalid() {
		validator = new Validator();
		assertFalse(validator.isDateValid("2018-09-32.12:00:00", dtFmt));
	}

	@Test
	public void testMonthIsInvalid() {
		validator = new Validator();
		assertFalse(validator.isDateValid("2018-13-20.12:00:00", dtFmt));
	}


	@Test
	public void testDateFormatIsInvalid() {
		validator = new Validator();
		assertFalse(validator.isDateValid("2018/01/20.12:00:00", dtFmt));
	}

	@Test
	public void testDateFeb29_2012() {
		validator = new Validator();
		assertTrue(validator.isDateValid("2012-02-29.12:00:00", dtFmt));
	}

	@Test
	public void testDateFeb29_2011() {
		validator = new Validator();
		assertFalse(validator.isDateValid("2011-02-29.12:00:00", dtFmt));
	}

	@Test
	public void testDateFeb28() {
		validator = new Validator();
		assertTrue(validator.isDateValid("2018-02-28.12:00:00", dtFmt));
	}

	@Test
	public void testDateIsValid_1() {
		validator = new Validator();
		assertTrue(validator.isDateValid("2018-01-31.12:00:00", dtFmt));
	}

	@Test
	public void testDateIsValid_2() {
		validator = new Validator();
		assertTrue(validator.isDateValid("2018-04-30.12:00:00", dtFmt));
	}

	@Test
	public void testDateIsValid_3() {
		validator = new Validator();
		assertTrue(validator.isDateValid("2018-09-30.12:00:00", dtFmt));
	}	
	
	
	
	@Test
	public void testHourInvalid_1() {
		validator = new Validator();
		assertFalse(validator.isDateValid("2018-09-30.24:00:00", dtFmt));
	}	

	@Test
	public void testMinuteInvalid_1() {
		validator = new Validator();
		assertFalse(validator.isDateValid("2018-09-30.00:61:00", dtFmt));
	}	
	@Test
	public void testSecondInvalid_1() {
		validator = new Validator();
		assertFalse(validator.isDateValid("2018-09-30.00:00:61", dtFmt));
	}	

	
	@Test
	public void testHourValid_1() {
		validator = new Validator();
		assertTrue(validator.isDateValid("2018-05-31.00:00:00", dtFmt));
	}	
	
	@Test
	public void testHourValid_2() {
		validator = new Validator();
		assertTrue(validator.isDateValid("2018-05-31.23:59:59", dtFmt));
	}
	
	
	
	@Test
	public void testDurationIsNull() {
		validator = new Validator();
		assertFalse(validator.isDurationValid(null));
	}	
	
	@Test
	public void testDurationInvalid() {
		validator = new Validator();
		assertFalse(validator.isDurationValid(""));
	}	
	
	@Test
	public void testDurationInvalid_1() {
		validator = new Validator();
		assertFalse(validator.isDurationValid("seconds"));
	}	
	
	@Test
	public void testDurationValid_1() {
		validator = new Validator();
		assertTrue(validator.isDurationValid("hourly"));
	}	
	@Test
	public void testDurationValid_2() {
		validator = new Validator();
		assertTrue(validator.isDurationValid("daily"));
	}	
	
	
	
/*	
	@Test
	public void testDateIsNull() {
		validator = new Validator();
		assertFalse(validator.isDateValid(null, "dd/MM/yyyy"));
	}

	@Test
	public void testDayIsInvalid() {
		validator = new Validator();
		assertFalse(validator.isDateValid("32/02/2012", "dd/MM/yyyy"));
	}

	@Test
	public void testMonthIsInvalid() {
		validator = new Validator();
		assertFalse(validator.isDateValid("31/20/2012", "dd/MM/yyyy"));
	}

	@Test
	public void testYearIsInvalid() {
		validator = new Validator();
		assertFalse(validator.isDateValid("31/20/19991", "dd/MM/yyyy"));
	}

	@Test
	public void testDateFormatIsInvalid() {
		validator = new Validator();
		assertFalse(validator.isDateValid("2012/02/20", "dd/MM/yyyy"));
	}

	@Test
	public void testDateFeb29_2012() {
		validator = new Validator();
		assertTrue(validator.isDateValid("29/02/2012", "dd/MM/yyyy"));
	}

	@Test
	public void testDateFeb29_2011() {
		validator = new Validator();
		assertFalse(validator.isDateValid("29/02/2011", "dd/MM/yyyy"));
	}

	@Test
	public void testDateFeb28() {
		validator = new Validator();
		assertTrue(validator.isDateValid("28/02/2011", "dd/MM/yyyy"));
	}

	@Test
	public void testDateIsValid_1() {
		validator = new Validator();
		assertTrue(validator.isDateValid("31/01/2012", "dd/MM/yyyy"));
	}

	@Test
	public void testDateIsValid_2() {
		validator = new Validator();
		assertTrue(validator.isDateValid("30/04/2012", "dd/MM/yyyy"));
	}

	@Test
	public void testDateIsValid_3() {
		validator = new Validator();
		assertTrue(validator.isDateValid("31/05/2012", "dd/MM/yyyy"));
	}
*/	
	
	

}
